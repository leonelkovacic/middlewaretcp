package client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Flow;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.SubmissionPublisher;
import java.util.concurrent.TimeUnit;

public class NioTest {

    private static int clientsCount = 2;

    public static void main(String[] args) throws Exception {
        NioTest test = new NioTest();
        new Thread(() -> {
            try {
                int client = 0;
                for (int i = 0; i < clientsCount; i++) {
                    test.addClient(client++, "Cliente_" + i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        test.run();

    }

    private Selector selector;
    private InetSocketAddress myAddr;

    private Map<Integer, SubmissionPublisher> connections = new HashMap<>();

    public NioTest() throws IOException {
        myAddr = new InetSocketAddress("localhost", 8080);
        selector = Selector.open();
    }

    private Flow.Publisher addClient(int channel, String message) throws IOException {

        MySubscriber mySubscriber = new MySubscriber(1000);

        SubmissionPublisher<Message> publisher =
                new SubmissionPublisher<>(ForkJoinPool.commonPool(), 10 * 1024 * 1024);
        publisher.subscribe(mySubscriber);

        SocketChannel client = SocketChannel.open(myAddr);
        client.configureBlocking(false);
        System.out.println("Registrando ...");
        client.register(selector, SelectionKey.OP_READ);
        System.out.println("Registrado !!");
        client.write(ByteBuffer.wrap(new Message(channel, message).toString().getBytes()));
        System.out.println("Msg Enviado !!");

        this.connections.put(channel, publisher);
        return publisher;
    }

    private void run() throws Exception {

        while (true) {
            System.out.println("Bucle");
            //TODO: improve this
            int readyCount = selector.select(4000);
            if (readyCount == 0) {
                continue;
            }
            Set<SelectionKey> readyKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = readyKeys.iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                // Remove key from set so we don't process it twice
                iterator.remove();
                // operate on the channel...
                if (key.isReadable()) {

                    SocketChannel client = (SocketChannel) key.channel();

                    // Read byte coming from the client
                    int BUFFER_SIZE = 1024;
                    ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
                    try {
                        client.read(buffer);
                        String rawMessage = new String(buffer.array()).trim();
                        Message message = MessageUtils.parseMessage(rawMessage);
                        if (!this.connections.containsKey(message.getChannelId())) {
                            throw new IllegalArgumentException("Unknown channel: " + message.getChannelId());
                        }
                        SubmissionPublisher publisher = this.connections.get(message.getChannelId());
                        int lag = publisher.offer(message, 2000, TimeUnit.MILLISECONDS, (subscriber, msg) -> {
                            System.out.println("A message has been dropped");
                            return false;
                        });
                        System.out.println("Dropped messages: " + lag);
                    } catch (Exception e) {
                        // client is no longer active
                        e.printStackTrace();
                        continue;
                    }
                }
            }
        }
    }
}
