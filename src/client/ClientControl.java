package client;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Flow;

public class ClientControl {

    private static ClientControl instance;

    private Map<Long, Flow.Subscriber> conections = new HashMap<Long, Flow.Subscriber>();
    private long lastId = 0;

    public static ClientControl getIntance() {
        if (instance == null) {
            instance = new ClientControl();
        }
        return instance;
    }


    public Long connect(Flow.Subscriber subscriber) throws UnknownHostException, IOException {
//		Socket socket = new Socket("localhost",8080);
        Long newId = this.lastId++;
        conections.put(newId, subscriber);
        return newId;
    }


}
