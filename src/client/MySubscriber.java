package client;

import java.util.concurrent.Flow;

public class MySubscriber implements Flow.Subscriber<Message> {

    private final int sleepTime;

    private Flow.Subscription subscription;
    private int totalRead = 0;

    public MySubscriber(int sleepTime) {
        this.sleepTime = sleepTime;
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1);
    }

    @Override
    public void onNext(Message item) {
        System.out.println("New message: " + item.getMessage() + " for channel: " + item.getChannelId());

        try {
            Thread.sleep(this.sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.totalRead++;

        subscription.request(1);
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void onComplete() {
        System.out.println("All messages received. Total messages: " + this.totalRead);
    }

}
