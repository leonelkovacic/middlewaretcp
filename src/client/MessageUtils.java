package client;

public class MessageUtils {

    public static Message parseMessage(String rawMessage) {
        String[] message = rawMessage.split("-");
        if (message.length != 2) {
            throw new IllegalArgumentException("Invalid message format.");
        }
        int channel = Integer.valueOf(message[0]);
        return new Message(channel, message[1]);
    }

}
